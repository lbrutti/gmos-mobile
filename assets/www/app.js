/**
 * File che contiene tutti gli include necessari al funzionamento
 * dell'applicazione.
 * E' responsabile del caricamento delle viste all'interno del Viewport
 */
/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
 */

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides
Ext.Loader.setPath({
	'Ext' : '~/Sites/touch',
	'GmosMobile' : 'app',
	// HighCharts Extension
	'Chart.ux' : './Chart/ux'
});
Ext.application({
			name : 'GmosMobile',

			requires : [ 'Ext.MessageBox', 'Ext.data.Store',
					'Chart.ux.Highcharts',
					'Ext.data.proxy.LocalStorage'
					],
			controllers : [ 'Login', 'Sensor', 'Main' ],
			views : [ 'Main', 'Login', 'Sensor' ],

			stores : [ 'GmosMobile.store.User' ],
			models : ['GmosMobile.model.User'],


			isIconPrecomposed : true,

			launch : function() {
			
				//aggiunge view
				Ext.Viewport.add([ {
					xtype : 'mainview'
				}, {
					xtype : 'loginview'
				}, {
					xtype : 'sensorview'
				} ]);

			},

			onUpdated : function() {
				Ext.Msg
						.confirm(
								"Application Update",
								"This application has just successfully been updated to the latest version. Reload now?",
								function(buttonId) {
									if (buttonId === 'yes') {
										window.location.reload();
									}
								});
			}
		});
