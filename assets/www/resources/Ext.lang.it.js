/**
 * Il file serve a tradurre il contenuto dei datepicker
 * 
 * @author Lorenzo Brutti
 */


Date.monthNames = [
    "Gennaio", "Febbraio", "Marzo",
    "Aprile", "Maggio", "Giugno",
    "Luglio", "Agosto", "Settembre",
    "Ottobre", "Novembre", "Dicembre"
];



if(Ext.picker.Date) {
	Ext.apply(Ext.DatePicker.prototype, {
		useTitles   : true,
		dayText		: 'Giorno',
		monthText	: 'Mese',
		yearText	: 'Anno'
	});
}