// versione ajax

Ext.define('GmosMobile.store.Temperature', {
  extend : 'Ext.data.Store',
  autoLoad : false,
  storeId: 'temperature',
  config: {
     proxy: {
    	 //sistemare...
       type : 'ajax',
       url : 'data/temp_exampleXXX.php',
       extraParams: { summary: 0 },
       reader : {
         type : 'json',
         rootProperty : 'rows'
       }
    }
  }
});


//versione localstorage

//Ext.define('GmosMobile.store.Temperature', {
//  extend : 'Ext.data.Store',
//  autoLoad : false,
//  storeId: 'temperatureStore',
//  config: {
//	  model : 'GmosMobile.model.Temperature',
//     proxy: {
//       type : 'localstorage',
//       id: 'tempertureProxy'
//       }
//    }
//  
//});