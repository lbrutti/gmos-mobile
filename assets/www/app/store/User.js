Ext.define('GmosMobile.store.User', {
	extend : 'Ext.data.Store',
	requires : [
	            'Ext.data.proxy.LocalStorage',
	            'GmosMobile.model.User'
	            ],
	config : {
		storeId : 'userStore',
		model : 'GmosMobile.model.User',
		proxy : {
			type : 'localstorage',
			id : 'userProxy'
		}
	}
});