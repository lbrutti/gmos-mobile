/**
 * Schermata iniziale da cui selezionare il sensore
 */

Ext.define(
				'GmosMobile.view.Main',
				{
					extend : 'Ext.Panel',
					xtype : 'main',
					alias : 'widget.mainview',
					
					requires : [ 'Ext.TitleBar', ],

					config : {
						layout : {
							type : 'fit'
						},
						
						items : [
								{
									docked : 'top',
									xtype : 'titlebar',
									title : 'Gmos Mobile',
									items : [ {
							//bottone di login, visible se l'utente non � loggato
										xtype : 'button',
										ui : 'action',
										align : 'right',
										text : 'Login',
										id : 'loginButton',
										hidden:false
									}, {
							//bottone di logout, visible se l'utente � loggato
										xtype : 'button',
										ui : 'action',
										align : 'right',
										text : 'Logout',
										id : 'logoutButton',
										hidden:true
									} ]
								},
								 {
									xtype : 'panel',
									items : [ {
										xtype : 'button',
										ui : 'action',
										align : 'center',
										text : 'Dati Mercurio',
										id : 'tekranButton',
										margin : '20px 20px 20px 20px'
									}, {
										xtype : 'button',
										ui : 'action',
										align : 'center',
										text : 'Dati Polveri',
										id : 'skypostButton',
										margin : '20px 20px 20px 20px'
									}, {
										xtype : 'button',
										ui : 'action',
										align : 'center',
										text : 'Dati MeteoH24',
										id : 'dataloggerH24Button',
										margin : '20px 20px 20px 20px'
									}, {
										xtype : 'button',
										ui : 'action',
										align : 'center',
										text : 'Dati MeteoMin10',
										id : 'dataloggerMin10Button',
										margin : '20px 20px 20px 20px'
									} ]
								},

						],
						listeners : [ {
							delegate : '#loginButton',
							event : 'tap',
							fn : 'onLogInButtonTap'
						},
						{
							delegate : '#logoutButton',
							event : 'tap',
							fn : 'onLogOutButtonTap'
						},
						{
							delegate : '#tekranButton',
							event: 'tap',
							fn : 'onTekranButtonTap'
						},
						{
							delegate : '#skypostButton',
							event: 'tap',
							fn : 'onSkypostButtonTap'
						},
						{
							delegate : '#dataloggerH24Button',
							event: 'tap',
							fn : 'onDataloggerH24ButtonTap'
						},
						{
							delegate : '#dataloggerMin10Button',
							event: 'tap',
							fn : 'onDataloggerMin10ButtonTap'
						},
						{
							event : 'show',
							fn : 'onMainViewShow'
						}]
					},//end config
					// devo caricare la view di login
					onLogInButtonTap : function() {
						var me = this; // riferimento alla view stessa
						me.fireEvent('showLogInPageCommand', me);
					},
					onLogOutButtonTap: function(){
						this.fireEvent('signOffCommand');
					},
					onTekranButtonTap : function (){
						this.fireEvent('tekranViewCommand');
					},
					onSkypostButtonTap : function (){
						this.fireEvent('skypostViewCommand');
					},
					onDataloggerMin10ButtonTap : function (){
						this.fireEvent('dataloggerMin10ViewCommand');
					},
					onDataloggerH24ButtonTap : function (){
						this.fireEvent('dataloggerH24ViewCommand');
					},
					onMainViewShow : function()
					{
						this.fireEvent('mainViewShowEvent');
					}
				});


