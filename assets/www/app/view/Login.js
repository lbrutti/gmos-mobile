Ext.define(
				'GmosMobile.view.Login',
				{
					extend : 'Ext.form.Panel',
					requires :[
					           'Ext.Img',
					           'Ext.Label',
					           'Ext.form.FieldSet',
					           'Ext.field.Password'
					           ],
					alias : "widget.loginview", // testare con apici singoli...
					config : {
						title : 'Login',
						items : [
								{
									xtype : 'image',
									src : Ext.Viewport.getOrientation() == 'portrait' ? 'resources/images/login.png'
											: 'resources/images/loginSmall.png',
									style : Ext.Viewport.getOrientation() == 'portrait' ? 'width:120px;height:120px;margin:auto'
											: 'width:40px;height:40px;margin:auto'
								},
								{
									xtype : 'label',
									html : 'Login Fallito. Controlla i dati inseriti...',
									itemId : 'signInFailedLabel',
									hidden : 'true',
									hideAnimation : 'fadeOut',
									showAnimation : 'fadeIn',
									style : 'color"#990000; margin:5px 0px;'
								}, {
									xtype : 'fieldset',
									title : 'Gmos Login',
									items : [ {
										xtype : 'textfield',
										placeHolder : 'Nome Utente',
										itemId : 'userNameTextField',
										name : 'userNameTextField',
										required : true
									}, {
										xtype : 'passwordfield',
										placeHolder : 'Password',
										itemId : 'passwordTextField',
										name : 'passwordTextField',
										required : true
									} ]
								}, {
									xtype : 'button',
									itemId : 'loginButton',
									ui : 'action',
									padding : '10px',
									text : 'Log In'
								},
								{
									xtype : 'button',
									itemId : 'cancelButton',
									ui : 'action',
									padding: '10px',
									text : 'Cancel'
								}],
								listeners:[{
									delegate: '#loginButton',
									event: 'tap',
									fn: 'onLogInButtonTap'
								},
								{
									delegate : '#cancelButton',
									event : 'tap',
									fn : 'onCancelButtonTap'
								}]
					},//end config
					
				//handler dell'evento LoginButton
					onLogInButtonTap : function(){
					var me = this; //riferimento alla view stessa
					
					var usernameField = me.down('#userNameTextField'),
						passwordField = me.down('#passwordTextField'),
						label		  =	me.down('#signInFailedLabel');
					
					label.hide();
					
					var username = usernameField.getValue(),
						password = passwordField.getValue();
					
					var task = Ext.create('Ext.util.DelayedTask', function(){
						label.setHtml('');
						me.fireEvent('signInCommand', me, username, password);

						usernameField.setValue('');
						passwordField.setValue('');
					});
					task.delay(500);//permette all'animazione di terminare
				},
				showSignInFailedMessage: function(message){
					var label = this.down('#signInFailedLabel');
					label.setHtml(message);
					label.show();
				},
				
				onCancelButtonTap : function(){
					this.fireEvent('cancelLoginCommand');
				}
});