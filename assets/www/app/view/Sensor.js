/**
 * View del sensore selezionato
 */

// variabili d'istanza
var fromDate = new Date();
var toDate = new Date();
fromDate.setDate(toDate.getDate() - 7);
	
var sensorName;
var sensorId;
var table;
var dato;
var user;

Ext.define('GmosMobile.view.Sensor', {
	extend : 'Ext.Panel',
	requires : [ 'Ext.TitleBar', 'Ext.field.DatePicker', 'Chart.ux.Highcharts',
			'Chart.ux.Highcharts.Serie' ],
	alias : 'widget.sensorview',
	config : {
		layout : {
			type : 'fit'
		},
		items : [ {// barra del titolo
			xtype : 'titlebar',
			id : 'titleBar',
			docked : 'top',
			items : [ {
				xtype : 'button',
				text : 'Back',
				itemId : 'backToHomeButton',
				align : 'left'
			} ]
		}, { // datapickers
			xtype : 'panel',
			docked : 'top',
			layout : 'vbox',
			
			margin : '1',
			items : [ {
				xtype : 'fieldset',
				id : 'datePickers',
				layout : 'hbox',
				items : [ {
					xtype : 'datepickerfield',
					name : 'fromDate',
					dateFormat : 'd/m/y',
					itemId : 'fromDate',
					label:'From',
					labelWidth: 'auto',
					value: fromDate,
					flex : 0.5,
					height: 10
				}, {
					xtype : 'datepickerfield',
					name : 'toDate',
					dateFormat : 'd/m/y',
					label:'To',
					labelWidth: 'auto',
					itemId : 'toDate',
					value: toDate,
					flex: 0.5,
					height:10,
					width: 60
				}
				 , {
				 xtype : 'button',
				 iconCls : 'refresh',
				 align : 'left',
				 itemId : 'refreshButton'
				 }
				, {
					// select per dati meteo, popolata dal controller
					xtype : 'selectfield',
					label : 'Dato',
					docked : 'top',
					margin : '5',
					labelWidth:'auto',
					id : 'selectDatoMeteo',
					options : [ ],
					hidden : true
				} ]
			} ]
		},

		{ // chart + tabella
			xtype : 'panel',
			layout : 'vbox',
			margin : '0 10 0 10',
			scrollable: {
			    direction: 'vertical',
			    directionLock: true
			},
			items : [
			         {
						xtype : 'panel',
						id : 'chartContainer',
//						flex: 2
					}, {
						xtype : 'panel',
						html : ' ',
						itemId : 'tableRenderPanel',
						id : 'tableContainer',
//						flex:1
					}
			]
		},
	
		],//end layout,
		
		listeners : [ {
			delegate : '#backToHomeButton',
			event : 'tap',
			fn : 'onBackToHomeButtonTap'
		}, {
			delegate : '#fromDate',
			event : 'change',
			fn : 'onFromDatePickerChange'
		}, {
			delegate : '#toDate',
			event : 'change',
			fn : 'onToDatePickerChange'
		}
		 , {
		 delegate : '#refreshButton',
		 event : 'tap',
		 fn : 'onRefreshButtontap'
		 }
		, {
			delegate : '#showPickersButton',
			event : 'tap',
			fn : 'onShowPickersButton'
		}, {
			delegate : '#selectDatoMeteo',
			event : 'change',
			fn : 'onMeteoDataSelected'
		},
		{
			delegate : '#nextRecords',
			event: 'tap',
			fn: 'onNextRecordsTap'
		},
		{
			delegate : '#prevRecords',
			event: 'tap',
			fn: 'onPrevRecordsTap'
		},
		{
			delegate : '#nextWindRecords',
			event: 'tap',
			fn: 'onNextWindRecordsTap'
		},
		{
			delegate : '#prevWindRecords',
			event: 'tap',
			fn: 'onPrevWindRecordsTap'
		}
		]
	},// end config

	onBackToHomeButtonTap : function() {
		this.fireEvent('backToHomeCommand');
	},
	onFromDatePickerChange : function() {
		fromDate = this.down('#fromDate').getValue().getTime();
	},
	onToDatePickerChange : function() {
		toDate = this.down('#toDate').getValue().getTime();

	},
	onRefreshButtontap : function() {
		if ((toDate < fromDate) || (!( toDate || fromDate) ))
			alert('Controlla le date!');
		else
			this.fireEvent('refreshDataCommand', fromDate / 1000,
				toDate / 1000, dato);
	},
	onMeteoDataSelected : function() {
		dato = this.down('#selectDatoMeteo').getValue();
	},
	//sparati dai pulsanti di paging
	onNextRecordsTap : function(){
		this.fireEvent('showNextRecordsCommand', 15);
	},
	onPrevRecordsTap : function(){
		this.fireEvent('showPrevRecordsCommand', -15);
	},
	//sparati dai pulsanti di paging della tabella dei venti
	onNextWindRecordsTap : function(){
		this.fireEvent('showNextWindRecordsCommand', 15);
	},
	onPrevWindRecordsTap : function(){
		this.fireEvent('showPrevWindRecordsCommand', -15);
	},
	// invocato dal controller
	setTitleBarTitle : function(title) {
		var titleBar = this.down('#titleBar');
		titleBar.setTitle(title);
	},

	// Configurazione standard per grafico non polare a linea
	getHighchartsConfig : function() {
		return {
			initAnimAfterLoad : false,
			chartConfig : {
				lang: {
					months: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
					weekdays: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
					shortMonths : [ 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dec' ]
					},
				chart : {
					showAxes : true,
					zoomType : 'x'
				},
				plotOptions : {
					series : {
						cursor : 'pointer'
					},
					line: {
				        marker: {
				            enabled: false
				        }
				    }
				},
				yAxis : {
					title : {text: 'Valore'}
				},
				xAxis : {
                    title : {
                        text : 'Data',
//                        type: 'datetime',
                        margin : 20
                    },
                    labels : {
                        rotation : 45,
                        y : 35,
                        formatter : function () {
                            var dt = Ext.Date.parse (parseInt (this.value) / 1000, "U");
                            if (dt) {
                                return Ext.Date.format (dt, "d-m H:i");
                            }
//                            return this.value;
                        }
                    }
                },
				legend : {
					enabled : false
				},
				tooltip : {
					formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y;
                }
				},
				title :{
					text: ''
				},
				credits : {
					enabled : false
				}

			}

		};

	},
	getHighchartsPolarConfig : function() {
		return {
			initAnimAfterLoad : false,
			chartConfig : {
				chart : {
					polar : true,
					type : 'column',
					showAxes: true
				},
				legend : {
					align : 'right',
					verticalAlign : 'top',
					y : 100,
					layout : 'vertical',
					title: 'Scala Beaufort'
				},
				xAxis : {
					categories : [ 'N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE',
							'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW',
							'NNW' ],
					labels : {
						rotation : 'auto'
					}
				},
				yAxis : {

				},
				tooltip : {
					followPointer : true
				},
				plotOptions : {
					series : {
						stacking : 'normal',
						shadow : false,
						groupPadding : 0,
						pointPlacement : 'on'
					}
				},
				title :{
					text: ''
				},
				credits : {
					enabled : false
				}

			}

		};

	}
});

