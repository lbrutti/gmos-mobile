/**
 * Superclasse per la gestione dei dati dai sensori
 */


//grafico
var chart;
//utente (anonimo o loggato)
var userStore;
var utente;
//variabile globale che salva la serie del grafico.
//necessaria per paging
var serieArray, windTableData;
//mantengono indice del primo e ultimo records visualizzato in tabella
var primoRecord = 0, ultimoRecord = 15;

//INSERIRE QUI nome dominio!
//var baseURL = 'http://151.95.6.223/~lbrutti/gmosWeb/';
//var baseURL = 'http://gmos2013.no-ip.org/';
var baseURL = 'http://gmos.dsi.unive.it/';
var chartSource = 'mobileApp/data/chart/';
var chartLoggerSource = 'mobileApp/data/chartLoggerApp/';
var sensorName;
var min10Table = 'min10';
var h24Table = 'h24';
var sensorsId = new Object();
sensorsId.Tekran=1;
sensorsId.Skypost=2;
sensorsId.MeteoMin10=3;
sensorsId.MeteoH24=4;


Ext.define(
				'GmosMobile.controller.Sensor',
				{
					extend : 'Ext.app.Controller',
					requires : [ 'Ext.data.JsonP',
					        'Chart.ux.Highcharts.Serie',
							'Chart.ux.Highcharts.SplineSerie',
							'Chart.ux.Highcharts.BarSerie',
							'Chart.ux.Highcharts.LineSerie', 
							'Ext.Array' ],
					config : {
						refs : {
							sensorView : 'sensorview',
							loggedMenuView : 'loggedmenuview',
							mainView : 'mainview'
						},
						control : {
							sensorView : {
								// viene registrata, per ogni view, la lista dei
								// comandi da ascoltare
								// con la relativa funzione
								backToHomeCommand : 'onBackToHomeCommand',
								refreshDataCommand : 'onRefreshDataCommand',
								showNextRecordsCommand : 'onShowNextRecordsCommand',
								showPrevRecordsCommand: 'onShowPrevRecordsCommand',
								showNextWindRecordsCommand : 'onShowNextWindRecordsCommand',
								showPrevWindRecordsCommand: 'onShowPrevWindRecordsCommand'
							},
							mainView	 : {
								tekranViewCommand : 'onTekranViewCommand',
								skypostViewCommand : 'onSkypostViewCommand',
								dataloggerMin10ViewCommand : 'onDataloggerMin10ViewCommand',
								dataloggerH24ViewCommand : 'onDataloggerH24ViewCommand'
							}
						}
					},
					// Transitions
					slideLeftTransition : {
						type : 'slide',
						direction : 'left'
					},
					slideRightTransition : {
						type : 'slide',
						direction : 'right'
					},

					/**
					 * Comandi associati agli eventi: devono far riferimento
					 * all'autenticazione dell'utente per invocare la funzione
					 * corretta
					 */

					/**
					 * carica la view dei dati del relativo sensore
					 */
					onTekranViewCommand : function() {
						
						sensorName = 'Tekran';
						this.showSensorView(sensorName);
					},
					onSkypostViewCommand : function() {
						sensorName = 'Skypost';
						this.showSensorView(sensorName);
					},
					onDataloggerMin10ViewCommand : function() {
						sensorName = 'Meteo min10';
						this.showSensorView(sensorName);
					},
					onDataloggerH24ViewCommand : function() {
						sensorName = 'Meteo h24';
						this.showSensorView(sensorName);
					},
					// comandi per eventi da SensorView:
					onBackToHomeCommand : function() {
						this.backToHomePage();
					},
					
					
					/**
					 * onRefreshDataCommand
					 * 
					 * Invoca la funzione di creazione del grafico in base al sensore 
					 * di cui viene passato il nome.
					 * livello d'accesso in base al livello utente
					 * 
					 * @author Lorenzo Brutti
					 *
					 * @param from
					 * @param to
					 * @param dato
					 */
					onRefreshDataCommand : function(from, to, dato) {
						switch (sensorName){
						case ('Tekran'):
							this.refreshSensorData(from, to, sensorsId.Tekran, utente.getLevel());
							break;
						case ('Skypost'):
							this.refreshSensorData(from, to, sensorsId.Skypost, utente.getLevel());
							break;
						case ('Meteo min10'):
							this.refreshLoggerData(from, to, sensorsId.MeteoMin10, utente.getLevel(), min10Table, dato);
							break;
						case ('Meteo h24'):							
							this.refreshLoggerData(from, to, sensorsId.MeteoH24, utente.getLevel(), h24Table, dato);
							break;
						
						default :
							break;
						}
					},
					onShowNextRecordsCommand : function (nextAmount){							
						this.createTable(Ext.getCmp('tableContainer'), serieArray, nextAmount);
					},
					onShowPrevRecordsCommand : function (nextAmount){
						this.createTable(Ext.getCmp('tableContainer'), serieArray, nextAmount);
					},
					
					onShowNextWindRecordsCommand : function (nextAmount){
						this.paginateWindTable(Ext.getCmp('tableContainer'), nextAmount);
					},
					onShowPrevWindRecordsCommand : function (nextAmount){
						this.paginateWindTable(Ext.getCmp('tableContainer'), nextAmount);
					},		
					// helper
					backToHomePage : function() {
						var mainMenu = this.getMainView();
						Ext.Viewport.animateActiveItem(mainMenu,
								this.slideRightTransition);
					},
					/**
					 * Funzione responsabile della creazione della vista
					 * relativa al sensore La vista contiene un grafico e una
					 * tabella
					 * 
					 * imposta la select dei dati per i dati meteo
					 * 
					 * @param sensorName:
					 *            titolo da impostare
					 * @param userLevel:
					 *            livello di accesso consentito all'utente
					 */
					showSensorView : function(sensorName, userLevel) {
						var sensorView = this.getSensorView();
						var selectMeteo = sensorView.down('#selectDatoMeteo');
//						var tabella = sensorView.down('#tableContainer').setHtml('');
						//qui carico l'utente
						userStore = Ext.getStore('userStore');
						if (userStore){
						utente = userStore.findRecord('username','Utente');
						}
						else{
							utente = Ext.create('GmosMobile.model.User');
							utente.set('username', 'Utente');
							utente.set('level', 1000);
							utente.set('logged', false);
							
						}
						switch (sensorName){
						case ('Meteo min10'):
							var min10Opt = [				                        
						                        {text:'Temp. Media Esterna', value:['AirTC_Avg','Temp. Media Esterna (C)'] },
						                        {text:'Temp. Media Interna', value:['T109_C_Avg','Temp. Media Interna (C)']},
						                        {text:'Wind Chill', value: ['WC_C_Avg', 'Wind Chill (C)']},
						                        {text:'Punto di rugiada', value:['TdC_Avg', 'Punto di Rugiada(C)']},
						                        {text:'Indice calore Medio', value:['HI_C_Avg','Indice Calor Medio (C)']},
						                        {text:"Umidita' Relativa", value:['RH_Max',"Umidita' Rel. (%)"]},
						                        {text:'Precipitazioni', value:['Rain_mm_Tot', 'Precipitazioni (mm)']},
						                        {text:'WindRose', value:['Venti', 'Windrose - Scala Beaufort']}				                              
						                    	];
							if (utente.getLevel() == 2)
								min10Opt.push({text:'Batteria CR200 (media)', value:['BattV_Avg', 'Carica media (V)']});
							selectMeteo.setOptions(min10Opt);
							selectMeteo.show();
							break;
						case ('Meteo h24'):
							var h24Opt = [
							              {text:'Temp. Media Esterna', value:['AirTC_Avg','Temp. Media Esterna (C)'] },
					                        {text:'Temp. Max Esterna', value:['AirTC_Max','Temp. Max Esterna (C)']},
					                        {text:'Temp. Min Esterna', value:['AirTC_Min','Temp. Min Esterna (C)']},
					                        {text:'Temp. Media Interna', value:['T109_C_Avg','Temp. Media Interna (C)']},
					                        {text:'Wind Chill', value: ['WC_C_Avg', ,'Wind Chill (C)']},
					                        {text:'Precipitazioni', value:['Rain_mm_Tot','Precipitazioni (mm)']},
					                        {text:'WindRose', value:['Venti', 'Windrose - Scala Beaufort']},
					                        ];
							selectMeteo.setOptions();
							if (utente.getLevel() == 2){
								h24Opt.push({text:'Batteria CR200 (min)', value:['BattV_Min', 'Carica media (V)']});
							}
							selectMeteo.setOptions(h24Opt);
							selectMeteo.show();
							break;
						default: 
							selectMeteo.hide();
							break;
						}

						sensorView.setTitleBarTitle('Gmos - ' + sensorName);
						
						//distrugge la tabella
						Ext.getCmp('tableContainer').setHtml('');
						Ext.getCmp('tableContainer').setItems([]);

						Ext.Viewport.animateActiveItem(sensorView,
								this.slideLeftTransition);

						// aggiunta per visualizzare chart quando carico la view
						// del sensore
						var chartContainer = Ext.getCmp('chartContainer');
						chart = chartContainer.getInnerItems();
						Ext.each(chart, function(data) {
							data.destroy();
						});
						chart = Ext.widget('highchart', this.getSensorView()
								.getHighchartsConfig());
						chartContainer.add([ chart ]).show();
						// fino qui!
					},
					
					/**
					 * Recupera dal server dell'applicazione web il JSON
					 * con il dato meteorologico richiesto
					 * @param from
					 * @param to
					 * @param idSensor
					 * @param level
					 * @param dbTable
					 * @param dato
					 */
					refreshLoggerData : function(from, to, idSensor, level, dbTable,
							dato) {
						var config;//configurazione del grafico
						var urlJsonp; //url del controller remoto da invocare
						var data = [];
						var datiVenti = [];//var per salvare dati della rosa dei venti
						serieArray = []; //var globale usata per salvare serie di grafici non polari
						var chartContainer = Ext.getCmp('chartContainer');//Elemento della view in cui renderizzare il grafico
						var tableContainer = Ext.getCmp('tableContainer');//Elemento della view in cui renderizzare la tabella
						chart = chartContainer.getInnerItems();
						//il vecchio grafico va distrutto prima di creare la nuova istanza
						Ext.each(chart, function(data) {
							if (data)
								data.destroy();
						});
						
						//si distinguono due casi: 
						//1. devo creare la rosa dei venti (grafico a coordinate polari)
						if (dato[0] == 'Venti'){
							datiVenti =['WindDir_D1_WVT','WS_ms_Avg'];
							config = this.getSensorView().getHighchartsPolarConfig();
							urlJsonp = baseURL + 'mobileApp/data/windRose/' + idSensor + '/' + level + '/' + from + '/'
							+ to + '/' + dbTable + '/' + datiVenti[0]+'/'+datiVenti[1] + '/callback';							
						}
						//2. devo creare una grafico a coordinate lineari
						else{
							config = this.getSensorView().getHighchartsConfig();
							urlJsonp = baseURL + chartLoggerSource + idSensor + '/' + level + '/' + from + '/'
								+ to + '/' + dbTable + '/' + dato[0] + '/callback';
						}
						chart = Ext.widget('highchart', config);
						
						Ext.data.JsonP
								.request({
									scope : this,
									url : urlJsonp,
									callbackName : 'callback',
									callback : function(result) {
									},
									timeout : 60000,
									failure : function(result) {
										alert("Impossibile recuperare il dato");
									},
									success : function(result) {
										if (dato[0] != 'Venti'){
											Ext.Array.each(result.data[0], function(element) {
												data.push(element);
											});
											serieArray = [ {
												name : dato[1],
												data : data,
											} ];		
											this.createTable(tableContainer, serieArray, 0, null);

										} else {
											var gradi = Object.keys(result.data);
											Ext.each(gradi, function(grado, index) {
												serieArray[index] = {
														name : grado,
														data : result.data[grado]
												};
											});
											windTableData = null;
											this.createWindTable(Ext.getCmp('tableContainer'),from, to, idSensor,level, dbTable, 0);
										}
										chart.addSeries(serieArray, true);
										chart.setTitle(this.getSensorView().down('#selectDatoMeteo').getValue()[1]);
										chartContainer.add([ chart ]).show();
										return;										
									}
								});
					},

					/**
					 * Aggiorna grafico e tabella di Tekran e Skypost
					 * @param from
					 * @param to
					 * @param idSensor
					 * @param level
					 */
					// recupera dati dal sito per i sensori Tekran e Skypost
					refreshSensorData : function(from, to, idSensor, level) {
						//pulisci serie
						serieArray = [];
						//pulisci container
						var chartContainer = Ext.getCmp('chartContainer');
						chart = chartContainer.getInnerItems();
						Ext.each(chart, function(data) {
							if (data)
								data.destroy();
						});
						chart = Ext.widget('highchart', this.getSensorView()
								.getHighchartsConfig());
						chartContainer.add([ chart ]).show();
						
						// qui deve essere fatta invocazione jsonp al server...
						var urlJsonp = baseURL+chartSource+ idSensor+'/'+level+'/'+from+'/'+to+'/callback';
						var data = [];
						var title, subtitle;
						switch (idSensor){
							case (1):
								title = 'Mercurio';
								subtitle = ' (ng/m3)';
							break;
							case (2):
								title = 'Polveri Sottili';
								subtitle = ' (ng/m3)';
							break;
							default:
								break;
								
						}
						// next we use Ext.data.JsonP to make a request
						Ext.data.JsonP.request({
							// we give it the url
							scope : this,
							url : urlJsonp,
							callbackName : 'callback',
							callback : function(result) {
							},
							timeout : 60000,
							failure : function(result) {
								alert("Impossibile recuperare il dato");
							},
							success : function(result) {
								Ext.Array.each(
										
												result.data[0],
												function(element) {
													data.push(element);
												});
								serieArray =  [ {
									name : title,
									data : data,
									type : 'line'
								} ];
								chart.addSeries(serieArray, true);
								chart.setTitle(title + subtitle);
								this.createTable(Ext.getCmp('tableContainer'), serieArray, 0, 'valore');
							}
						});
					},
					/**
					 * crea tabella usando la serie visuallizzata nel grafico
					 * e aggiunge pulsanti per paging dei risultati
					 * @param container = contenitore della view in cui renderizzare
					 * @param serie = serie da usare
					 * @param righe = quanti mostrare alla volta
					 */
					createTable : function (container, serie, righe, intestazione){
						
						var rows = "";
						var table ="<table>";
						var avanti, indietro, pannello;
						//aggiorno contatori record
						primoRecord += righe;
						ultimoRecord += righe;

						if (primoRecord < 0 || ultimoRecord <=0){
							primoRecord = 0;
							ultimoRecord = 15;
							return;
						}
						
						
						if (primoRecord>=0 && ultimoRecord >=0){
						table += "<th>TIMESTAMP</th>";
						Ext.each(serie, function (obj){
							if(intestazione)
								table += "<th>"+intestazione+"</th>";
							else
								table += "<th>"+obj.name+"</th>";
							Ext.each(obj.data, function(dati, counter){
								if (counter>primoRecord && counter<ultimoRecord){

									rows += "<tr>";
									Ext.each(dati, function(dato){
										//controllo per evitare di fare parsing su dati
										if (dato > 10000){
											var giorno = Ext.Date.parse(parseInt(dato) / 1000,"U");
											if (giorno){
												giorno = Ext.Date.format(giorno	, "Y-m-d H:i:s");
												rows +="<td>"+giorno+"</td>";
											}
										}else{
												rows +="<td>"+dato+"</td>";
												}
									});
									rows+= "</tr>";
								}
							});	
						});
						table += rows;
						table += "</table>";
						
						avanti = {
								xtype : 'button',
								text : 'Next 15',
								id : 'nextRecords',
								align : 'right',
								ui:'action'
							};
						indietro = {
								xtype : 'button',
								text : 'Prev. 15',
								id : 'prevRecords',
								align : 'left',
								ui:'action'
							};
						pannello = {
								xtype : 'panel',
								layout: 'hbox',
								items : [indietro,avanti]
							};
						container.setItems([pannello]);
						container.setHtml(table);
						}
					},
					/**
					 * crea la tabella con TS, velocita' media del vento e sua direzione
					 * @param container
					 * @param serie
					 * @param righe
					 * @param intestazione  container, serie, righe, intestazione
					 */
					createWindTable : function (container,from, to, idSensor, level, dbTable, righe){
						var urlGetData = baseURL + 'mobileApp/data/getWindData/' + idSensor + '/' + level + '/' + from + '/'
						+ to + '/' + dbTable + '/callback';	
						var table = "<table><th>TIMESTAMP</th><th>Vel. Media</th><th>Direzione</th>";
						var avanti, indietro, pannello;
						primoRecord += righe;
						ultimoRecord += righe;
						if (windTableData == null){
							console.log('windTAbleDAta == null');
							Ext.data.JsonP
							.request({
								scope : this,
								url : urlGetData,
								callbackName : 'callback',
								callback : function(result) {
								},
								timeout : 60000,
								failure : function(result) {
									alert("Impossibile recuperare il dato");
								},
								success : function(result) {
									windTableData = result;
									Ext.each(result.data, function(dato, counter){
										if (counter>primoRecord && counter<ultimoRecord){
											table+= "<tr>";
											Ext.each(dato, function(tripla){
												table+="<td>"+tripla+"</td>";
											});
											table+= "</tr>";
										}
									});
									
								table +="</table>";
								container.setHtml(table);
								
								}
							});
						}
						else {
							console.log ('windTableData != null')
							Ext.each(windTableData.data, function(dato, counter){
								if (counter>primoRecord && counter<ultimoRecord){
									table+= "<tr>";
									Ext.each(dato, function(tripla){
										table+="<td>"+tripla+"</td>";
									});
									table+= "</tr>";
								}
							});
							table +="</table>";
							container.setHtml(table);
						}
						avanti = {
								xtype : 'button',
								text : 'Next 15',
								id : 'nextWindRecords',
								align : 'right',
								ui:'action'
							};
						indietro = {
								xtype : 'button',
								text : 'Prev. 15',
								id : 'prevWindRecords',
								align : 'left',
								ui:'action'
							};
						pannello = {
								xtype : 'panel',
								layout: 'hbox',
								items : [indietro,avanti]
							};
						container.setItems([pannello]);
					},
					
					/**
					 * paginazione per la tabelle dei venti
					 * metodo ausiliario necessario per non eseguire una chiamata in piu'
					 * al server.
					 * 
					 * @param container : dove ricreare la tabella
					 * @param amount : numero di record da paginare 
					 */
					paginateWindTable : function (container, amount){
						var table = "<table><th>TIMESTAMP</th><th>Vel. Media</th><th>Direzione</th>";
						primoRecord += amount;
						ultimoRecord+= amount;
						if (primoRecord < 0 || ultimoRecord <=0){
							primoRecord = 0;
							ultimoRecord = 15;
							return;
						}
							
						Ext.each(windTableData.data, function(dato, counter){
							if (counter>primoRecord && counter<ultimoRecord){
								console.log('Counter ' + counter);
								table+= "<tr>";
								Ext.each(dato, function(tripla){
									table+="<td>"+tripla+"</td>";
								});
								table+= "</tr>";
							}
						});
						table +="</table>";
						container.setHtml(table);
					}
				});

				