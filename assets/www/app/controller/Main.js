/**
 * Questo controller controlla lo stato dell'utente all'avvio dell'applicazione
 * e alle successive visualizzazioni della main View, nel corso di una singola 
 * sessione di utilizzo
 * @author lbrutti
 */

Ext.define('GmosMobile.controller.Main',{
	extend: 'Ext.app.Controller',
	requires : [ 'GmosMobile.store.User',
	             'GmosMobile.model.User'],
	config :{
		refs :{
			mainView : 'mainview'
		},
		control: {
			mainView:{
				mainViewShowEvent : 'onMainViewShowEvent'
			}
		}
	},
	onMainViewShowEvent: function (){
		var store = Ext.getStore('userStore');
		if (! store)
			store = Ext.create('GmosMobile.store.User');
		var utente = store.findRecord('username','Utente');
		if(! utente){
			utente = Ext.create('GmosMobile.model.User');
			utente.set('username', 'Utente');
			utente.set('level', 1000);
			utente.set('logged', false);
			store.add(utente);
			store.sync();
		}
	}

});