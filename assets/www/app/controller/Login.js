/**
 * Controller responsabile delle funzioni di login.
 * Riceve gli eventi dalla mainView, dalla loginView e dal menu dei loggati.
 * Invoca la visualizzazione di:
 * 	 							LoginView dalla mainView
 * 								LoggedView dalla loginView
 * 								MainView dalla loggedView quando si esegue Logout
 * @author lbrutti
 */

Ext.define('GmosMobile.controller.Login',{
	extend: 'Ext.app.Controller',
	requires : 'GmosMobile.store.User',
	config: {
		refs: {
			mainView : 'mainview',
			loginView: 'loginview',
		},
		control :{
			mainView :{
				//evento emesso in corrispondenza della pressione del tasto Login della home
				showLogInPageCommand: 'onShowLogInPageCommand',
				signOffCommand: 'onSignOffCommand'
			},
			loginView:{
				//evento emesso in corrispondenza della pressione del tasto di invio dati
				signInCommand : 'onSignInCommand',
				cancelLoginCommand : 'onCancelLoginCommand',
			}
		}
	},// end config
	// Transitions
	slideLeftTransition : {
		type : 'slide',
		direction : 'left'
	},
	slideRightTransition : {
		type : 'slide',
		direction : 'right'
	},
	//Comandi
	/**
	 * invoca la visualizzazione della pagina di login dalla mainView
	 */
	onShowLogInPageCommand : function(view) {
		this.activateLogInPage();
	},
	onCancelLoginCommand : function(){
		this.backToHomePage();
	},
	
	/**
	 * GEstisce il login connettendosi al server per l'autenticazione
	 */
	onSignInCommand : function(view, username, password){
		var me = this;
		var	loginView = me.getLoginView();
		var urlLogin = baseURL+'mobileApp/verifylogin/login/' + username+'/'+password+'/callback';

		if (username.length === 0 || password.length === 0){
			loginView.showSignInFailedMessage('Inserisci Username e Password.');
			return;
		}
		loginView.setMasked({
			xtype:'loadmask',
			message: 'Logging...'
		});
		//invio dati (url encoded...) per il login
		Ext.data.JsonP.request({
        	scope: this,
            url: urlLogin,
            callbackName: 'callback',
            callback: function(result){
            },
            success: function(result) {
            	//se l'utente 
            	if (result.data.logged){
            		//se i dati sono corretti salva l'utente nel localstorage
            		//e richiama il menu per loggati
            		userStore = Ext.getStore('userStore');
            		
            		if (! userStore){
            			userStore = Ext.create('GmosMobile.store.User');
            		}
            		user = userStore.findRecord('username', 'Utente');
            		if (! user){
            			user = Ext.create('GmosMobile.model.User');
            			user.set('username', 'Utente');
            		}
           			user.set('level', result.data.level);
           			user.set('logged', result.data.logged);
            		
            		userStore.add(user);
            		userStore.sync();
            		
            		this.signInSuccess();

            	}
            	else{
            		loginView.showSignInFailedMessage('LOGIN NON RIUSCITO! CONTROLLA I DATI');
            		loginView.setMasked(false);
            		return;
            	}
            }
        });
	},


	signInSuccess : function(){
		var loginView = this.getLoginView();
		var loggedMenuView = this.getMainView();
		loginView.setMasked(false);//rimuove la maschera di logging
		
		//imposto la visibilita' dei bottoni di login/logout e gli handler
		loggedMenuView.down('#loginButton').hide();
		loggedMenuView.down('#logoutButton').show();
//		loggedMenuView.down('#webcamButton').show();
		
		Ext.Viewport.animateActiveItem(loggedMenuView, this.slideLeftTransition);
		return;
	},
	signInFailure: function(message){
		var loginView = this.getLoginView();
		loginView.showSigniInFailedMessage(message);
		loginView.setMasked(false);
	},
	/**
	 * rimuove l'utente dallo store e lo distrugge
	 */
	onSignOffCommand:function(){
		var mainView = this.getMainView();
		mainView.down('#loginButton').show();
		mainView.down('#logoutButton').hide();
		userStore = Ext.getStore('userStore');
		

		userStore.remove(user);
		userStore.sync();
		
		
		user.destroy();

		Ext.Viewport.animateActiveItem(this.getMainView(), this.slideRightTransition);
	},


	//helpers
	/**
	 * Helper per la visualizzazione della schermata di login
	 */
	activateLogInPage : function(){
		var loginPage = this.getLoginView();
		Ext.Viewport.animateActiveItem(loginPage, this.slideLeftTransition);
	},
	backToHomePage : function (){
		var homePage = this.getMainView();
		Ext.Viewport.animateActiveItem(homePage, this.slideRightTransition);
	}
});

var user;
var userStore;