Ext.define('GmosMobile.model.User', {
	   extend: 'Ext.data.Model',
	   config :{
	     fields: [
	         {name: 'username', type: 'string'},
	         {name: 'logged', type: 'Boolean'},
	         {name: 'level',  type: 'int'}
	     ],
	     idProperty : 'username'
	   },
	     isLogged : function(){
//	    	 console.log ('User.isLogged()=' +  this.get('logged'));

	    	 return this.get('logged');
	     },
	     getUsername : function(){
//	    	 console.log ('User.getUsername()=' +  this.get('username'));
	    	 return this.get('username');
	     },
	     /**
	      * restituisce livello dell'utente in modo da poterlo usare nei metodi
	      * di recupero dei dati
	      */
	     getLevel : function (){
	    	 var level =  this.get('level');
//	    	 console.log ('User.getLevel()=' +  this.get('level'));
	    	 if (level > 100)
	    		 return 1;
	    	 if (level < 100)
	    		 return 2;
	     },
	});